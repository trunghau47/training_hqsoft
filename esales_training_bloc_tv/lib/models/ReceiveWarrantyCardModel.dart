// ignore: file_names
class ReceiveWarrantyCardModel {
  String codeWarranty;
  String address;
  String phone;
  int statusCode;
  String statusName;
  String createDate;
  String content;
  ReceiveWarrantyCardModel({
    required this.codeWarranty,
    required this.address,
    required this.phone,
    required this.statusCode,
    required this.statusName,
    required this.createDate,
    required this.content,
  });
}

class StatusModel {
  int statusCode;
  String statusName;
  bool isSelected;
  StatusModel({
    required this.statusCode,
    required this.statusName,
    required this.isSelected,
  });
}
