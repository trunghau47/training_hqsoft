import 'package:esales_training/Screens/Home/home.dart';
import 'package:esales_training/Screens/Home/home_bloc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'models/data.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "App Jokee",
      home: Provider<FillterDateTime>.value(
        value: FillterDateTime(),
        child: const HomeScreen(),
      ),
    );
  }
}

// class Receive extends ChangeNotifier {
//   var data = Data();
//   void receiveWarranty([String? datetime]) {
//     if (datetime == null) {
//       final _receiveWarranty = Data();
//       // var a = _receiveWarranty.recevice;
//     } else {}
//     notifyListeners();
//   }
// }
