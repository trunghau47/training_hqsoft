import 'dart:async';
import 'package:esales_training/models/data.dart';
import 'package:esales_training/models/ReceiveWarrantyCardModel.dart';

class FillterDateTime {
  var data = Data().recevice;

  StreamController<List<ReceiveWarrantyCardModel>> fillterController =
      StreamController<List<ReceiveWarrantyCardModel>>();
  // StreamController<List<ReceiveWarrantyCardModel>> fillterController =
  //     StreamController<List<ReceiveWarrantyCardModel>>();

  fillter(String dateTimeFrom, String dateTimeEnd) {
    if (dateTimeFrom.isEmpty && dateTimeEnd.isEmpty) {
      fillterController.sink.add(data);
      return;
    }
    _fillter(dateTimeFrom, dateTimeEnd)
        .then((value) => fillterController.sink.add(value));
  }

  Future<List<ReceiveWarrantyCardModel>> _fillter(
      String dateTimeFrom, String dateTimeEnd) {
    var c = Completer<List<ReceiveWarrantyCardModel>>();
    List<ReceiveWarrantyCardModel> list = [];
    List<ReceiveWarrantyCardModel> filteredData = Data()
        .recevice
        .where((item) =>
            DateTime.parse(item.createDate)
                    .isAfter(DateTime.parse(dateTimeFrom)) &&
                DateTime.parse(item.createDate)
                    .isBefore(DateTime.parse(dateTimeEnd)) ||
            DateTime.parse(item.createDate)
                .isAtSameMomentAs(DateTime.parse(dateTimeEnd)) ||
            DateTime.parse(item.createDate)
                .isAtSameMomentAs(DateTime.parse(dateTimeFrom)))
        .toList();
    list = filteredData;
    c.complete(list);
    return c.future;
  }

  search(String query) {
    if (query.isEmpty) {
      fillterController.sink.add(data);
      return;
    }
    _searchFillter(query).then((value) => fillterController.add(value));
  }

  Future<List<ReceiveWarrantyCardModel>> _searchFillter(String query) {
    var c = Completer<List<ReceiveWarrantyCardModel>>();
    List<ReceiveWarrantyCardModel> list = [];

    data.forEach((element) {
      if (element.codeWarranty.contains(query)) {
        list.add(element);
      }
    });
    c.complete(list);
    return c.future;
  }

  fillterStatus(int status) {
    if (status == 0) {
      fillterController.sink.add(data);
      return;
    }
    _fillterStatus(status).then((value) => fillterController.add(value));
  }

  Future<List<ReceiveWarrantyCardModel>> _fillterStatus(int status) {
    var c = Completer<List<ReceiveWarrantyCardModel>>();
    List<ReceiveWarrantyCardModel> list = [];
    list = Data()
        .recevice
        .where((element) => element.statusCode == status)
        .toList();
    print(list);
    c.complete(list);
    return c.future;
  }

  dispose() {
    fillterController.close();
  }
}
