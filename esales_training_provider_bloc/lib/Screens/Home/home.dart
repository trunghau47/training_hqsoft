// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import '../../models/ReceiveWarrantyCardModel.dart';
import '../../models/data.dart';
import 'home_bloc.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final searchController = TextEditingController();
  late TextEditingController dateFromController = TextEditingController();
  late TextEditingController dateEndController = TextEditingController();
  late TextEditingController statusController = TextEditingController();
  late String dateTimeFrom = DateFormat('yyyy-MM-dd').format(DateTime.now());
  late String dateTimeEnd = DateFormat('yyyy-MM-dd').format(DateTime.now());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    var bloc = Provider.of<FillterDateTime>(context);
    searchController.addListener(() {
      bloc.search(searchController.text);
    });
    dateEndController.addListener(() {
      bloc.fillter(dateFromController.text, dateEndController.text);
    });
    statusController.addListener(() {
      bloc.fillterStatus(int.parse(statusController.text));
    });
  }

  final _status = StatusData();

  var isSelected =
      StatusData().status.firstWhere((element) => element.isSelected == true);

  late int yearStart = 2019;
  late int monthStart = 1;
  late int dayStart = 1;

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    // print("Test Line 64: || $isSelected");
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[800],
        title: const Text("Danh Sách Phiếu"),
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          left: 10,
          right: 10,
        ),
        child: Column(
          children: [
            DateTimeWidget(screenWidth, context),
            StatusWidget(context),
            SearchWidget(),
            ReceiveWidget(screenWidth),
          ],
        ),
      ),
    );
  }

  Consumer<FillterDateTime> SearchWidget() {
    return Consumer<FillterDateTime>(
      builder: (context, bloc, child) => Row(
        children: [
          Icon(
            Icons.search,
            color: Colors.blue[800],
          ),
          const SizedBox(width: 10),
          Expanded(
            child: TextFormField(
              controller: searchController,
              decoration: const InputDecoration(
                hintText: "Nhập Thông Tin Để Tìm Kiếm",
                hintStyle: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
          )
        ],
      ),
    );
  }

  Consumer<FillterDateTime> StatusWidget(BuildContext context) {
    return Consumer<FillterDateTime>(
      builder: (context, bloc, child) => Row(
        children: [
          const Expanded(
            flex: 1,
            child: Text(
              "Trạng Thái",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Expanded(
            flex: 3,
            child: TextFormField(
              decoration: InputDecoration(
                hintText: isSelected.statusName,
                suffixIcon: IconButton(
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return StatefulBuilder(
                          builder: (context, StateSetter setState) {
                            return AlertDialog(
                              title: FractionallySizedBox(
                                widthFactor: 1,
                                child: Container(
                                  height: 70,
                                  color: Colors.blue[800],
                                  child: const Center(
                                    child: Text(
                                      'Chọn trạng thái',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              contentPadding: const EdgeInsets.symmetric(
                                  horizontal: 0, vertical: 20),
                              content: SizedBox(
                                // color: Colors.amber,
                                width: double.maxFinite,
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: _status.status.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return ListTile(
                                      trailing: Checkbox(
                                        value: _status.status[index].isSelected,
                                        // _status.status[index].isSelected,
                                        onChanged: (bool? value) {
                                          setState(
                                            () {
                                              for (var element
                                                  in _status.status) {
                                                element.isSelected = false;
                                              }
                                              _status.status[index].isSelected =
                                                  true;
                                              isSelected =
                                                  _status.status[index];
                                            },
                                          );
                                        },
                                      ),
                                      title: Text(
                                        _status.status[index].statusName,
                                      ),
                                    );
                                  },
                                ),
                              ),
                              actions: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    ElevatedButton(
                                      onPressed: () {
                                        var status = _status.status
                                            .where((element) =>
                                                element.isSelected == true)
                                            .toList()
                                            .first;
                                        Navigator.of(context)
                                            .pop(status.statusCode);
                                      },
                                      style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.blue),
                                        minimumSize:
                                            MaterialStateProperty.all<Size>(
                                                const Size(150, 40)),
                                      ),
                                      child: const Text('Lưu'),
                                    ),
                                    const SizedBox(width: 30),
                                    ElevatedButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.grey),
                                        minimumSize:
                                            MaterialStateProperty.all<Size>(
                                                const Size(150, 40)),
                                      ),
                                      child: const Text('Đóng'),
                                    ),
                                  ],
                                )
                              ],
                            );
                          },
                        );
                      },
                    ).then((value) {
                      if (value != null) {
                        setState(() {
                          // _receiveWarranty = value;
                          statusController.text = value.toString();
                        });
                      }
                    });
                  },
                  icon: const Icon(Icons.arrow_drop_down),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Consumer<FillterDateTime> DateTimeWidget(
      double screenWidth, BuildContext context) {
    return Consumer<FillterDateTime>(
      builder: (context, bloc, child) => Row(
        children: [
          SizedBox(
            width: screenWidth / 2.2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  "Từ",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Expanded(
                  child: TextFormField(
                    textAlign: TextAlign.center,
                    enabled: false,
                    controller: dateFromController,
                    decoration: const InputDecoration(
                      hintStyle: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                ),
                IconButton(
                  onPressed: () {
                    DatePicker.showDatePicker(
                      context,
                      showTitleActions: true,
                      minTime: DateTime(2019, 1, 1),
                      maxTime: DateTime(2222, 1, 1),
                      onConfirm: (date) {
                        setState(() {
                          dateTimeFrom = DateFormat('yyyy-MM-dd').format(date);
                          dateFromController.text =
                              DateFormat('yyyy-MM-dd').format(date);
                          var split = date.toString().split("-");
                          yearStart = int.parse(split[0]);
                          monthStart = int.parse(split[1]);
                          // dayStart = int.parse(split[2]);
                        });
                      },
                      currentTime: DateTime.now(),
                      locale: LocaleType.vi,
                    );
                  },
                  icon: const Icon(Icons.calendar_today),
                ),
              ],
            ),
          ),
          SizedBox(
            width: screenWidth / 2.2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  "Đến",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Expanded(
                  child: TextFormField(
                    textAlign: TextAlign.center,
                    enabled: false,
                    controller: dateEndController,
                    decoration: const InputDecoration(
                      hintStyle: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                ),
                IconButton(
                  onPressed: () {
                    DatePicker.showDatePicker(
                      context,
                      showTitleActions: true,
                      minTime: DateTime(yearStart, monthStart, dayStart),
                      maxTime: DateTime(2222, 1, 1),
                      onConfirm: (date) {
                        var check = DateTime.parse(
                            DateFormat('yyyy-MM-dd').format(date));
                        if (check.isAfter(DateTime.parse(dateTimeFrom)) ||
                            check.isAtSameMomentAs(
                                DateTime.parse(dateTimeFrom))) {
                          setState(() {
                            dateTimeEnd = DateFormat('yyyy-MM-dd').format(date);
                            dateEndController.text =
                                DateFormat('yyyy-MM-dd').format(date);
                          });
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                              content: Text(
                                  "Ngày Kết Thúc Không Được Nhỏ Hơn Ngày Bắt Đầu"),
                            ),
                          );
                        }
                      },
                      currentTime: DateTime.now(),
                      locale: LocaleType.vi,
                    );
                  },
                  icon: const Icon(Icons.calendar_today),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Consumer<FillterDateTime> ReceiveWidget(double screenWidth) {
    return Consumer<FillterDateTime>(
      builder: (context, bloc, child) => Expanded(
        child: Padding(
          padding: const EdgeInsets.only(top: 10),
          child: StreamBuilder<List<ReceiveWarrantyCardModel>>(
            initialData: [],
            stream: bloc.fillterController.stream,
            builder: (context, snapshot) {
              return ListView.builder(
                physics: const BouncingScrollPhysics(),
                shrinkWrap: true,
                itemCount: snapshot.data?.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    color: index % 2 == 0 ? Colors.grey[200] : Colors.white,
                    width: screenWidth,
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          "assets/svg/icons8-document.svg",
                          height: 40,
                          width: 40,
                        ),
                        SizedBox(
                          width: screenWidth - 70,
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  const Text(
                                    "Mã Phiếu: ",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    snapshot.data![index].codeWarranty,
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  const Text(
                                    "Địa Chỉ: ",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  Expanded(
                                    child: Text(
                                      snapshot.data![index].address,
                                      softWrap: true,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  const Text(
                                    "Ngày: ",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    snapshot.data![index].createDate,
                                    softWrap: true,
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  const Text(
                                    "Nội Dung: ",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    // "ámda",
                                    snapshot.data![index].content,
                                    softWrap: true,
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  const Text(
                                    "Trạng Thái: ",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    snapshot.data![index].statusName,
                                    style: TextStyle(
                                      color: snapshot.data![index].statusCode ==
                                              1
                                          ? Colors.amber[600]
                                          : snapshot.data![index].statusCode ==
                                                  2
                                              ? Colors.green[700]
                                              : Colors.red,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    softWrap: true,
                                  ),
                                ],
                              ),
                              const Divider(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              );
            },
          ),
        ),
      ),
    );
  }
}
