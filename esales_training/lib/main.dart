import 'package:esales_training/Screens/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'models/data.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "App Jokee",
      home: HomeScreen(),
    );
  }
}

// class Receive extends ChangeNotifier {
//   var data = Data();
//   void receiveWarranty([String? datetime]) {
//     if (datetime == null) {
//       final _receiveWarranty = Data();
//       // var a = _receiveWarranty.recevice;
//     } else {}
//     notifyListeners();
//   }
// }
