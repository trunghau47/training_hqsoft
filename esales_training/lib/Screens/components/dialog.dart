import 'package:flutter/material.dart';

import '../../models/ReceiveWarrantyCardModel.dart';

class StatusDialog extends StatefulWidget {
  final List<StatusModel> statusList;

  const StatusDialog({Key? key, required this.statusList}) : super(key: key);

  @override
  _StatusDialogState createState() => _StatusDialogState();
}

class _StatusDialogState extends State<StatusDialog> {
  late List<StatusModel> _status;

  @override
  void initState() {
    super.initState();
    _status = List.from(widget.statusList);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Container(
        height: 60,
        width: double.maxFinite,
        color: Colors.blue[800],
        child: const Text(
          'Chọn trạng thái',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      content: SizedBox(
        width: double.maxFinite,
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: _status.length,
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
              trailing: Checkbox(
                value: _status[index].isSelected,
                onChanged: (bool? value) {
                  setState(() {
                    for (var element in _status) {
                      element.isSelected = false;
                    }
                    _status[index].isSelected = true;
                  });
                },
              ),
              title: Text(_status[index].statusName),
            );
          },
        ),
      ),
      actions: <Widget>[
        ElevatedButton(
          onPressed: () => Navigator.of(context).pop(),
          child: const Text('Đóng'),
        ),
        ElevatedButton(
          onPressed: () {
            // Lấy trạng thái đã chọn
            StatusModel selectedStatus =
                _status.firstWhere((element) => element.isSelected);
            // Tiếp tục xử lý ở đây...

            // Đóng dialog và cập nhật lại trạng thái
            Navigator.of(context).pop(selectedStatus);
          },
          child: const Text('Lưu'),
        ),
      ],
    );
  }
}
