import 'ReceiveWarrantyCardModel.dart';

class Data {
  late List<ReceiveWarrantyCardModel> recevice;
  Data() {
    recevice = [];
    recevice.add(ReceiveWarrantyCardModel(
        codeWarranty: "Phieu-112233",
        address: "72/24 Phan Đăng Lưu Bình Thạnh",
        phone: "0327123123",
        statusCode: 1,
        statusName: "Đã Tiếp Nhận",
        createDate: "2022-04-18",
        content: "Kiểm Tra Máy Tính Hư Ổ Cứng"));
    recevice.add(ReceiveWarrantyCardModel(
        codeWarranty: "Phieu-223344",
        address: "29 Nguyễn Cửu Vân, Thảo Điền, Quận 2, Tp. HCM",
        phone: "0903123456",
        statusCode: 2,
        statusName: "Đã Hoàn Tất",
        createDate: "2022-04-19",
        content: "Laptop không khởi động được"));

    recevice.add(ReceiveWarrantyCardModel(
        codeWarranty: "Phieu-334455",
        address: "12 Nguyễn Văn Linh, Quận 7, Tp. HCM",
        phone: "0987123456",
        statusCode: 1,
        statusName: "Đã Tiếp Nhận",
        createDate: "2022-04-20",
        content: "Thay pin Laptop"));

    recevice.add(ReceiveWarrantyCardModel(
        codeWarranty: "Phieu-445566",
        address: "34 Lê Văn Sỹ, Quận 3, Tp. HCM",
        phone: "0978123456",
        statusCode: 2,
        statusName: "Đã Hoàn Tất",
        createDate: "2022-04-22",
        content: "Cài đặt phần mềm cho máy tính"));

    recevice.add(ReceiveWarrantyCardModel(
        codeWarranty: "Phieu-556677",
        address: "21 Trương Định, Quận 3, Tp. HCM",
        phone: "0988123456",
        statusCode: 3,
        statusName: "Đã Hủy",
        createDate: "2022-04-23",
        content: "Không cần sửa chữa nữa"));

    recevice.add(ReceiveWarrantyCardModel(
        codeWarranty: "Phieu-667788",
        address: "17 Điện Biên Phủ, Quận Bình Thạnh, Tp. HCM",
        phone: "0905123456",
        statusCode: 1,
        statusName: "Đã Tiếp Nhận",
        createDate: "2022-04-24",
        content: "Màn hình laptop bị vỡ"));

    recevice.add(ReceiveWarrantyCardModel(
        codeWarranty: "Phieu-778899",
        address: "45 Lê Lợi, Quận 1, Tp. HCM",
        phone: "0908123456",
        statusCode: 2,
        statusName: "Đã Hoàn Tất",
        createDate: "2022-04-25",
        content: "Laptop không kết nối được với Wifi"));

    recevice.add(ReceiveWarrantyCardModel(
        codeWarranty: "Phieu-889900",
        address: "122 Trần Hưng Đạo, Quận 1, Tp. HCM",
        phone: "0989123456",
        statusCode: 1,
        statusName: "Đã Tiếp Nhận",
        createDate: "2022-04-26",
        content: "Máy tính bị lỗi font chữ"));

    recevice.add(ReceiveWarrantyCardModel(
        codeWarranty: "Phieu-778499",
        address: "567/12 Lý Thái Tổ Quận 10",
        phone: "0123456789",
        statusCode: 3,
        statusName: "Đã Hủy",
        createDate: "2022-04-20",
        content: "Máy Không Sử Dụng Được"));
    recevice.add(ReceiveWarrantyCardModel(
        codeWarranty: "Phieu-445166",
        address: "123/4 Lê Văn Việt Quận 9",
        phone: "0987654321",
        statusCode: 2,
        statusName: "Đã Hoàn Tất",
        createDate: "2023-04-22",
        content: "Nâng Cấp RAM"));
  }
}

class StatusData {
  late List<StatusModel> status;
  StatusData() {
    status = [];
    status.add(
        StatusModel(statusCode: 0, statusName: "Tất Cả", isSelected: true));
    status.add(StatusModel(
        statusCode: 1, statusName: "Đã Tiếp Nhận", isSelected: false));
    status.add(StatusModel(
        statusCode: 2, statusName: "Đã Hoàn Tất", isSelected: false));
    status.add(
        StatusModel(statusCode: 3, statusName: "Đã Hủy", isSelected: false));
  }
}

class Test {
  late List<ReceiveWarrantyCardModel> test;
  Test() {
    test = [];

    ///
    test.add(ReceiveWarrantyCardModel(
        codeWarranty: "Phieu-12311223",
        address: "72/24 Phan Đăng Lưu Bình Thạnh",
        phone: "0327123123",
        statusCode: 1,
        statusName: "Đã Tiếp Nhận",
        createDate: "2022-04-18",
        content: "Kiểm Tra Máy Tính Hư Ổ Cứng"));
    test.add(ReceiveWarrantyCardModel(
        codeWarranty: "Phieu-12231233",
        address: "72/24 Phan Đăng Lưu Bình Thạnh",
        phone: "0327123123",
        statusCode: 1,
        statusName: "Đã Tiếp Nhận",
        createDate: "2022-04-18",
        content: "Kiểm Tra Máy Tính Hư Ổ Cứng"));
    test.add(ReceiveWarrantyCardModel(
        codeWarranty: "Phieu-122333",
        address: "72/24 Phan Đăng Lưu Bình Thạnh",
        phone: "0327123123",
        statusCode: 1,
        statusName: "Đã Tiếp Nhận",
        createDate: "2022-04-18",
        content: "Kiểm Tra Máy Tính Hư Ổ Cứng"));
    test.add(ReceiveWarrantyCardModel(
        codeWarranty: "Phieu-124233",
        address: "72/24 Phan Đăng Lưu Bình Thạnh",
        phone: "0327123123",
        statusCode: 1,
        statusName: "Đã Tiếp Nhận",
        createDate: "2022-04-18",
        content: "Kiểm Tra Máy Tính Hư Ổ Cứng"));
    test.add(ReceiveWarrantyCardModel(
        codeWarranty: "Phieu-1112233",
        address: "72/24 Phan Đăng Lưu Bình Thạnh",
        phone: "0327123123",
        statusCode: 1,
        statusName: "Đã Tiếp Nhận",
        createDate: "2022-04-18",
        content: "Kiểm Tra Máy Tính Hư Ổ Cứng"));
    test.add(ReceiveWarrantyCardModel(
        codeWarranty: "Phieu-1112233",
        address: "72/24 Phan Đăng Lưu Bình Thạnh",
        phone: "0327123123",
        statusCode: 1,
        statusName: "Đã Tiếp Nhận",
        createDate: "2022-04-18",
        content: "Kiểm Tra Máy Tính Hư Ổ Cứng"));
  }
}
