// ignore_for_file: non_constant_identifier_names
import 'bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import '../../models/ReceiveWarrantyCardModel.dart';
import '../../models/data.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<FillterBloc>(context).add(LoadEvent());
  }

  final _status = StatusData();
  var isSelected =
      StatusData().status.firstWhere((element) => element.isSelected == true);

  late int yearStart = 2019;
  late int monthStart = 1;
  late int dayStart = 1;

  List<ReceiveWarrantyCardModel> _list = [];

  final searchController = TextEditingController();
  late TextEditingController dateFromController = TextEditingController();
  late TextEditingController dateEndController = TextEditingController();
  late TextEditingController statusController = TextEditingController();
  late String dateTimeFrom = DateFormat('yyyy-MM-dd').format(DateTime.now());
  late String dateTimeEnd = DateFormat('yyyy-MM-dd').format(DateTime.now());
  double screenWidth = 0;

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    // print("Test Line 64: || $isSelected");
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[800],
        title: const Text("Danh Sách Phiếu"),
      ),
      body: BlocListener<FillterBloc, FillterState>(
        listener: (context, state) {
          if (state is LoadedState) {
            _list = state.list;
          } else if (state is FillDateTimeState) {
            _list = state.list;
          } else if (state is FillStatusState) {
            _list = state.list;
          } else if (state is FillSearchState) {
            _list = state.list;
          }
        },
        child: BlocBuilder<FillterBloc, FillterState>(
          builder: (context, state) {
            return _bodyWiget();
          },
        ),
      ),
    );
  }

  Widget _bodyWiget() {
    return RefreshIndicator(
      onRefresh: () async {
        // context.read<FillterBloc>().add(PullToRefreshEvent());
        BlocProvider.of<FillterBloc>(context).add(PullToRefreshEvent());
      },
      child: Column(
        children: [
          ///*DateTime
          dateTimeWidget(screenWidth, context),

          ///* Status
          statusWidget(context),

          ///*Search
          serchWidget(context),

          ///*Receiver

          (_list.isNotEmpty)
              ? receiveWidget(screenWidth)
              : const Center(
                  child: Padding(
                    padding: EdgeInsets.only(top: 50),
                    child: Text("List has no elements"),
                  ),
                ),
        ],
      ),
    );
  }

  Expanded receiveWidget(double screenWidth) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(top: 15),
        child: SingleChildScrollView(
          child: Column(
            children: [
              ListView.builder(
                physics: const BouncingScrollPhysics(),
                shrinkWrap: true,
                itemCount: _list.length,
                itemBuilder: (context, index) {
                  final list = _list[index];

                  if (_list.isNotEmpty) {
                    return Container(
                      color: index % 2 == 0 ? Colors.grey[200] : Colors.white,
                      width: screenWidth,
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            "assets/svg/icons8-document.svg",
                            height: 40,
                            width: 40,
                          ),
                          SizedBox(
                            width: screenWidth - 70,
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    const Text(
                                      "Mã Phiếu: ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      list.codeWarranty,
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    const Text(
                                      "Địa Chỉ: ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Expanded(
                                      child: Text(
                                        list.address,
                                        softWrap: true,
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    const Text(
                                      "Ngày: ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      list.createDate,
                                      softWrap: true,
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    const Text(
                                      "Nội Dung: ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      // "ámda",
                                      list.content,
                                      softWrap: true,
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    const Text(
                                      "Trạng Thái: ",
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      list.statusName,
                                      style: TextStyle(
                                        color: list.statusCode == 1
                                            ? Colors.amber[600]
                                            : list.statusCode == 2
                                                ? Colors.green[700]
                                                : Colors.red,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      softWrap: true,
                                    ),
                                  ],
                                ),
                                const Divider(),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  } else {
                    return const Center(
                      child: Text("Không Tìm Thấy"),
                    );
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Row serchWidget(BuildContext context) {
    return Row(
      children: [
        Icon(
          Icons.search,
          color: Colors.blue[800],
        ),
        const SizedBox(width: 10),
        Expanded(
          child: TextFormField(
            onChanged: (value) {
              setState(
                () {
                  Provider.of<FillterBloc>(context, listen: false)
                      .add(FillSearchEvent(query: searchController.text));
                },
              );
            },
            controller: searchController,
            decoration: const InputDecoration(
              hintText: "Nhập Thông Tin Để Tìm Kiếm",
              hintStyle: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
        )
      ],
    );
  }

  Row statusWidget(BuildContext context) {
    return Row(
      children: [
        const Expanded(
          flex: 1,
          child: Text(
            "Trạng Thái",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        Expanded(
          flex: 3,
          child: TextFormField(
            decoration: InputDecoration(
              hintText: isSelected.statusName,
              suffixIcon: IconButton(
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return StatefulBuilder(
                        builder: (context, StateSetter setState) {
                          return AlertDialog(
                            title: FractionallySizedBox(
                              widthFactor: 1,
                              child: Container(
                                height: 70,
                                color: Colors.blue[800],
                                child: const Center(
                                  child: Text(
                                    'Chọn trạng thái',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            contentPadding: const EdgeInsets.symmetric(
                                horizontal: 0, vertical: 20),
                            content: SizedBox(
                              width: double.maxFinite,
                              child: ListView.builder(
                                shrinkWrap: true,
                                itemCount: _status.status.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return ListTile(
                                    trailing: Checkbox(
                                      value: _status.status[index].isSelected,
                                      onChanged: (bool? value) {
                                        setState(
                                          () {
                                            for (var element
                                                in _status.status) {
                                              element.isSelected = false;
                                            }
                                            _status.status[index].isSelected =
                                                true;
                                            isSelected = _status.status[index];
                                          },
                                        );
                                      },
                                    ),
                                    title: Text(
                                      _status.status[index].statusName,
                                    ),
                                  );
                                },
                              ),
                            ),
                            actions: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  ElevatedButton(
                                    onPressed: () {
                                      var status = _status.status
                                          .where((element) =>
                                              element.isSelected == true)
                                          .toList()
                                          .first;
                                      Navigator.of(context)
                                          .pop(status.statusCode);
                                    },
                                    style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.blue),
                                      minimumSize:
                                          MaterialStateProperty.all<Size>(
                                              const Size(150, 40)),
                                    ),
                                    child: const Text('Lưu'),
                                  ),
                                  const SizedBox(width: 30),
                                  ElevatedButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.grey),
                                      minimumSize:
                                          MaterialStateProperty.all<Size>(
                                              const Size(150, 40)),
                                    ),
                                    child: const Text('Đóng'),
                                  ),
                                ],
                              )
                            ],
                          );
                        },
                      );
                    },
                  ).then(
                    (value) {
                      if (value != null) {
                        setState(
                          () {
                            Provider.of<FillterBloc>(context, listen: false)
                                .add(
                              FillStatusEvent(
                                statusCode: int.parse(
                                  value.toString(),
                                ),
                              ),
                            );
                          },
                        );
                      }
                    },
                  );
                },
                icon: const Icon(Icons.arrow_drop_down),
              ),
            ),
          ),
        )
      ],
    );
  }

  Row dateTimeWidget(double screenWidth, BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: screenWidth / 2.2,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                "Từ",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Expanded(
                child: TextFormField(
                  textAlign: TextAlign.center,
                  enabled: false,
                  controller: dateFromController,
                  decoration: const InputDecoration(
                    hintStyle: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ),
              ),
              IconButton(
                onPressed: () {
                  DatePicker.showDatePicker(
                    context,
                    showTitleActions: true,
                    minTime: DateTime(2019, 1, 1),
                    maxTime: DateTime(2222, 1, 1),
                    onConfirm: (date) {
                      setState(() {
                        dateTimeFrom = DateFormat('yyyy-MM-dd').format(date);
                        dateFromController.text =
                            DateFormat('yyyy-MM-dd').format(date);
                        var split = date.toString().split("-");
                        yearStart = int.parse(split[0]);
                        monthStart = int.parse(split[1]);
                        // ! Error, not set day in library Datatime picker
                        // dayStart = int.parse(split[2]);
                      });
                    },
                    currentTime: DateTime.now(),
                    locale: LocaleType.vi,
                  );
                },
                icon: const Icon(Icons.calendar_today),
              ),
            ],
          ),
        ),
        SizedBox(
          width: screenWidth / 2.2,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                "Đến",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              Expanded(
                child: TextFormField(
                  textAlign: TextAlign.center,
                  enabled: false,
                  controller: dateEndController,
                  decoration: const InputDecoration(
                    hintStyle: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ),
              ),
              IconButton(
                onPressed: () {
                  DatePicker.showDatePicker(
                    context,
                    showTitleActions: true,
                    minTime: DateTime(yearStart, monthStart, dayStart),
                    maxTime: DateTime(2222, 1, 1),
                    onConfirm: (date) {
                      var check =
                          DateTime.parse(DateFormat('yyyy-MM-dd').format(date));
                      if (check.isAfter(DateTime.parse(dateTimeFrom)) ||
                          check
                              .isAtSameMomentAs(DateTime.parse(dateTimeFrom))) {
                        setState(
                          () {
                            dateTimeEnd = DateFormat('yyyy-MM-dd').format(date);
                            dateEndController.text =
                                DateFormat('yyyy-MM-dd').format(date);
                            Provider.of<FillterBloc>(context, listen: false)
                                .add(
                              FillDateTimeEvent(
                                dateTimeFrom: dateFromController.text,
                                dateTimeEnd: dateEndController.text,
                              ),
                            );
                          },
                        );
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text(
                                "Ngày Kết Thúc Không Được Nhỏ Hơn Ngày Bắt Đầu"),
                          ),
                        );
                      }
                    },
                    currentTime: DateTime.now(),
                    locale: LocaleType.vi,
                  );
                },
                icon: const Icon(Icons.calendar_today),
              ),
            ],
          ),
        )
      ],
    );
  }
}
