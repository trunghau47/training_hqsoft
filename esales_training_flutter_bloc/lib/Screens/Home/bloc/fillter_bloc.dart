import 'package:esales_training/Screens/Home/bloc/fillter_event.dart';
import 'package:esales_training/Screens/Home/bloc/flitter_state.dart';

import 'package:esales_training/models/data.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../models/ReceiveWarrantyCardModel.dart';

// class FillterBloc extends Bloc<FillterEvent, FillterState> {
//   var list = Data().recevice;

//   FillterBloc({required this.list}) : super(FillterInitial()) {
//     print(list.length);
//     on<FillterEvent>(
//       (event, emit) {
//         if (event is LoadEvent || event is PullToRefreshEvent) {
//           emit(LoadingState());
//           try {
//             final fillter = list;
//             emit(LoadedState(list: list));
//           } catch (e) {
//             emit(FailureLoadState(message: e.toString()));
//           }
//         }
//       },
//     );
//   }
// }

class FillterBloc extends Bloc<FillterEvent, FillterState> {
  FillterBloc() : super(FillterInitial()) {
    on<FillterEvent>(
      (event, emit) {
        if (event is LoadEvent) {
          List<ReceiveWarrantyCardModel> list = [];

          list = Data().recevice;

          emit(LoadedState(list: list));
        } else if (event is PullToRefreshEvent) {
          List<ReceiveWarrantyCardModel> list = [];

          list = Data().recevice;

          emit(LoadedState(list: list));
        } else if (event is PressItemListEvent) {
          var change = event.text + ' 1213';
          emit(ChangeTextState(text: change));
        } else if (event is FillDateTimeEvent) {
          List<ReceiveWarrantyCardModel> list = [];
          list = Data().recevice;
          try {
            var value = list
                .where((item) =>
                    DateTime.parse(item.createDate)
                            .isAfter(DateTime.parse(event.dateTimeFrom)) &&
                        DateTime.parse(item.createDate)
                            .isBefore(DateTime.parse(event.dateTimeEnd)) ||
                    DateTime.parse(item.createDate)
                        .isAtSameMomentAs(DateTime.parse(event.dateTimeEnd)) ||
                    DateTime.parse(item.createDate)
                        .isAtSameMomentAs(DateTime.parse(event.dateTimeFrom)))
                .toList();
            emit(FillDateTimeState(list: value));
          } on Exception catch (e) {
            // ignore: avoid_print
            print('Error FillDateTimeEvent: $e');
          }
        } else if (event is FillStatusEvent) {
          List<ReceiveWarrantyCardModel> list = [];
          list = Data().recevice;
          List<ReceiveWarrantyCardModel> value;
          if (event.statusCode == 0) {
            value = list;
          } else {
            value = list
                .where((element) => element.statusCode == event.statusCode)
                .toList();
          }
          emit(FillStatusState(list: value));
        } else if (event is FillSearchEvent) {
          List<ReceiveWarrantyCardModel> list = [];
          list = Data().recevice;
          List<ReceiveWarrantyCardModel> value;
          if (event.query.isEmpty) {
            value = list;
          } else {
            value = list
                .where((element) =>
                    element.codeWarranty.contains(event.query) ||
                    element.content.contains(event.query))
                .toList();
          }
          emit(FillSearchState(list: value));
        }
      },
    );
  }
}
