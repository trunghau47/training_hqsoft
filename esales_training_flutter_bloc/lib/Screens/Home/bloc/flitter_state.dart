// ignore_for_file: public_member_api_docs, sort_constructors_first
// part of 'fillter_bloc.dart';
import 'package:esales_training/models/ReceiveWarrantyCardModel.dart';

abstract class FillterState {}

class FillterInitial extends FillterState {}

class LoadingState extends FillterState {}

class LoadedState extends FillterState {
  final List<ReceiveWarrantyCardModel> list;
  LoadedState({required this.list});
}

class FailureLoadState extends FillterState {
  String message;
  FailureLoadState({required this.message});
}

class ChangeTextState extends FillterState {
  String text;
  ChangeTextState({
    required this.text,
  });
}

class FillDateTimeState extends FillterState {
  // String dateTimeFrom;
  // String dateTimeTo;
  // ListDateTimeState({required this.dateTimeFrom, required this.dateTimeTo});
  List<ReceiveWarrantyCardModel> list;
  FillDateTimeState({
    required this.list,
  });
}

class FillStatusState extends FillterState {
  List<ReceiveWarrantyCardModel> list;
  FillStatusState({
    required this.list,
  });
}

class FillSearchState extends FillterState {
  List<ReceiveWarrantyCardModel> list;
  FillSearchState({
    required this.list,
  });
}
