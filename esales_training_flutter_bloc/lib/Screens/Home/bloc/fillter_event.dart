// ignore_for_file: public_member_api_docs, sort_constructors_first
abstract class FillterEvent {}

class LoadEvent extends FillterEvent {}

class PullToRefreshEvent extends FillterEvent {}

class PressItemListEvent extends FillterEvent {
  String text;
  PressItemListEvent({
    required this.text,
  });
}

class FillDateTimeEvent extends FillterEvent {
  String dateTimeFrom;
  String dateTimeEnd;
  FillDateTimeEvent({required this.dateTimeFrom, required this.dateTimeEnd});
}

class FillStatusEvent extends FillterEvent {
  int statusCode;
  FillStatusEvent({
    required this.statusCode,
  });
}

class FillSearchEvent extends FillterEvent {
  String query;
  FillSearchEvent({
    required this.query,
  });
}
