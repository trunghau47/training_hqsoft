import 'package:esales_training/Screens/Home/bloc/fillter_bloc.dart';
import 'package:esales_training/Screens/Home/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Esales Training",
      home: BlocProvider(
        create: (context) => FillterBloc(),
        child: const HomeScreen(),
      ),
    );
  }
}
